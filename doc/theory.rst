Theory
------

Brief overview of the equation of the kQEq part of the code. For more details see the original paper.

Training on Electrostatic Properties
====================================

The prediction of the electronegativities of *N* atoms in our GPR implementation is defined as a matrix-vector multiplication:

.. math ::
        \mathbf{\chi}=\mathbf{K}_{NM}\mathbf{w}

The charge vector (including the Lagrange multiplier) can now easily be computed as

.. math ::
        \bar{\mathbf{q}} = - \bar{\mathbf{A}}^{-1} \bar{{\chi}},

Regression weights then can be obtained by taking a derivative of the loss function for some general electrostatic property, e.g. dipoles:

.. math ::
        \mathcal{L}_t = ||\mathbf{t}-\mathbf{t}_{\mathrm{ref}}||^2_{\mathbf{\Sigma}^{-1}_t}+ ||\mathbf{w}||_{\mathbf{K}_{MM}}^2 =||\mathbf{T}_t\mathbf{A}^{-1}(\mathbf{X}\mathbf{K}_{NM}\mathbf{w}-\mathbf{q}_{\mathrm{tot}})-\mathbf{t}_{\mathrm{ref}}||^2_{\mathbf{\Sigma}^{-1}_t}+ ||\mathbf{w}||_{\mathbf{K}_{MM}}^2

In this loss function, both the least-squares and regularization terms are quadratic in the regression weights. The optimal weights are obtained by taking its derivative, setting it to zero and solving for regression weights.

Training on Energies
====================================
TODO, please refer to the paper