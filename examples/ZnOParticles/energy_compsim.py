import sys
import random
import matplotlib.pyplot as plt
from kqeq.qeq import charge_eq
from kqeq.kernel import *
from kqeq.funct import *
from kqeq.kQEq import kQEq

import ase
from ase.units import Hartree
from ase import Atoms
from ase.io import read, write

import numpy as np
from sklearn.metrics import mean_squared_error
import math

atom_energy = {"Zn": -49117.02929728, "O": -2041.3604,
               "H": -13.63393, "Li": -7.467060138769*Hartree}

hard_lib = {"O": 1, "Zn": 1, "H": 0, "Li":0}



desdictSOAP = {"nmax": 6,
           "lmax": 5,
           "rcut": 3.0,
           "sigma": 3.0/8,
           "species": ["Zn","O"],
           "periodic": False}


SOAP_Kernel = SOAPKernel(multi_SOAP=False,
                         perEl = False,
                     descriptor_dict=desdictSOAP)


# Create an instance of the kQEq class

my_kqeq = kQEq(Kernel=SOAP_Kernel,
                     scale_atsize=1.0/np.sqrt(2),
                     radius_type="qeq",
                     hard_lib = hard_lib)

my_kqeq.load_kQEq()

test_set4 = read("particles5.xyz@:", format="extxyz")[:200] 
E_test4=[]
F_test4 = []
ref_f_test4 = []
for a in test_set4:
    res = my_kqeq.calculateEnergy(a)
    E_test4.append(res/len(a))
ref_en_test4 = get_energies_perAtom(mols=test_set4,atom_energy = atom_energy)


plt.figure(figsize=(10,8), dpi=100)
plt.plot(ref_en_test4,ref_en_test4,alpha = 0.5,color="black")
plt.scatter(ref_en_test4,E_test4,alpha = 0.5)
plt.xticks(fontsize=18)
plt.yticks(fontsize=18)
plt.show()

