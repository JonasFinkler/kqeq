import sys
import random
import matplotlib.pyplot as plt

from kqeq.qeq import charge_eq
from kqeq.kernel import *
from kqeq.kQEq import kernel_qeq
from kqeq.funct import *

import ase
from ase.units import Hartree
from ase import Atoms
from ase.io import read, write

import numpy as np
from sklearn.metrics import mean_squared_error
import math


# Commented structures are the ones used for the training and evaluation of the final model in the paper. This requires larger amount of RAM and the training time 
'''
train_set = read("particles.xyz@:",format="extxyz")
temp = read("particles1.xyz@:", format="extxyz")
train_set.extend(temp[:50])
temp = read("particles2.xyz@:", format="extxyz")
train_set.extend(temp[:100])
temp = read("particles3.xyz@:", format="extxyz")
train_set.extend(temp[:200])
temp = read("particles4.xyz@:", format="extxyz")
train_set.extend(temp[:200])

test_set = read("particles5.xyz@:", format="extxyz")[:200]
'''
##########################################################
temp = read("particles4.xyz@:",format="extxyz")
train_set = temp[:50]
test_set = temp[50:80]



atom_energy = {"Zn": -49117.02929728, "O": -2041.3604,
               "H": -13.63393, "Li": -7.467060138769*Hartree}

hard_lib = {"O": 1, "Zn": 1, "H": 0, "Li":0}



desdictSOAP = {"nmax": 6,
           "lmax": 5,
           "rcut": 3.0,
           "sigma": 3.0/8,
           "species": ["Zn","O"],
           "periodic": False}


SOAP_Kernel = SOAPKernel(multi_SOAP=False,
                     descriptor_dict=desdictSOAP,
                     training_set=train_set,
                     sparse=True,
                     sparse_method="CURel",
                     sparse_count=500,
                     perEl = False,
)

# Energy of the training set
ref_en_train = get_energies(mols=train_set,atom_energy = atom_energy)

# Create an instance of the kQEq class
my_kqeq = kernel_qeq(Kernel=SOAP_Kernel,
                     scale_atsize=1.0/np.sqrt(2),
                     radius_type="qeq",
                     hard_lib=hard_lib)

my_kqeq.trainEnCharge(atom_energy=atom_energy,
                      charge_keyword="hirshfeld", 
                      lambda_reg = 0.01,
                      lambda_charges = 0.01,
                      lambda_charges_min=0.001,
                      iter_charges = 2)

my_kqeq.save_kQEq()

ref_en_test = get_energies_perAtom(mols=test_set,atom_energy = atom_energy)
E_test=[]
for a in test_set:
    E_test.append(my_kqeq.calculateEnergy(a)/len(a))
plot_basics(ref = ref_en_test,kqeq = E_test,xlim=[-4.5,-3.3],ylim=[-4.5,-3.3],preset="energy")


