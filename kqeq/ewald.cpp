#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <iostream>
// g++ -O3 -Wall -shared -std=c++11 -fPIC $(python -m pybind11 --includes) ewald.cpp -o ewaldCPP$(python-config --extension-suffix)

namespace py = pybind11;

py::array_t<double> ewaldReal(
                        int nat,
                        py::array_t<double> pos, 
                        py::array_t<double> latPy, 
                        py::array_t<double> sigmaPy, 
                        py::array_t<double> hardPy, 
                        py::array_t<double> nPy,
                        double eta, 
                        double cutoff){

    py::buffer_info bufhard1 = hardPy.request();
    double *hard = (double*) bufhard1.ptr;

    py::buffer_info bufsigma1 = sigmaPy.request();
    double *sigma = (double*) bufsigma1.ptr;

    py::buffer_info buflat1 = latPy.request();
    double *lat = (double*) buflat1.ptr;

    py::buffer_info bufats1 = pos.request();
    double *ats = (double*) bufats1.ptr;

    py::buffer_info bufn1 = nPy.request();
    double *n = (double*) bufn1.ptr;


    py::array_t<double> Areal = py::array_t<double>(nat*nat);
    py::buffer_info buf1 = Areal.request();
    double *ptr1 = (double *) buf1.ptr;
    double dlat[3],d[3],d2,r, interf,invsqrt2eta, gamma;
    invsqrt2eta = 1 / (sqrt(2) * eta);
    const double PI =3.14159265359;
    for (int i = 0; i < nat*nat ; i++)
    {
        ptr1[i] = 0.0;
    }
    int lim1 = n[0]+1;
    // #pragma omp parallel for
    for (int iat=0; iat <nat; iat++){
        for (int i=-n[0]; i < lim1; i++){
            for (int j=-n[1]; j < n[1]+1; j++){
                for (int k=-n[2]; k < n[2]+1; k++){


                    dlat[0] = i*lat[0] + j*lat[3] +k*lat[6];
                    dlat[1] = i*lat[1] + j*lat[4] +k*lat[7];
                    dlat[2] = i*lat[2] + j*lat[5] +k*lat[8];

                    // std::cout << dlat[0] << " " << dlat[1] << " " << dlat[2] << std::endl;
                    for (int jat=iat;jat<nat;jat++){
                        if (i != 0 || j != 0 || k!=0 || iat!=jat){
                            d[0] = ats[iat*3] - ats[jat*3] + dlat[0];
                            d[1] = ats[iat*3+1] - ats[jat*3+1] + dlat[1];
                            d[2] = ats[iat*3+2] - ats[jat*3+2] + dlat[2];
                            d2 = d[0]*d[0] + d[1]*d[1] + d[2]*d[2];

                            if (d2 > cutoff*cutoff){
                                continue;
                            }

                            r = sqrt(d2);
                                                     
                            interf = erfc(r*invsqrt2eta);
                            
                            if (sigma[0] > 0){
                                gamma = sqrt(sigma[iat]*sigma[iat]+sigma[jat]*sigma[jat]);
                                interf = interf - erfc(r/(sqrt(2)*gamma));}
                            ptr1[iat+nat*jat] = ptr1[iat+nat*jat] + interf/r;}                            
        }}}}                        
        if (sigma[0] > 0){
            ptr1[nat*iat+iat] = ptr1[nat*iat+iat] + 1/(sigma[iat]*sqrt(PI)) + hard[iat];
            }
    }
    for (int iat = 0; iat < nat; iat ++){
        for (int jat = 0; jat < nat; jat ++){
            ptr1[jat+nat*iat] = ptr1[iat+nat*jat];}}
    Areal.resize({nat,nat});
    return Areal;
}


py::array_t<double> ewaldRecip(
                        int nat,
                        py::array_t<double> pos, 
                        py::array_t<double> reclatPy, 
                        py::array_t<double> nPy,
                        double eta, 
                        double cutoff,
                        double V){

    py::buffer_info buflat1 = reclatPy.request();
    double *reclat = (double*) buflat1.ptr;

    py::buffer_info bufats1 = pos.request();
    double *ats = (double*) bufats1.ptr;

    py::buffer_info bufn1 = nPy.request();
    double *n = (double*) bufn1.ptr;


    py::array_t<double> Arecip = py::array_t<double>(nat*nat);
    py::buffer_info buf1 = Arecip.request();
    double *ptr1 = (double *) buf1.ptr;
    

    double dlat[3],r, factor, kri,krj;

    for (int i = 0; i < nat*nat ; i++)
    {
        ptr1[i] = 0.0;
    }
    int lim1 = n[0]+1;
    // #pragma omp parallel for
    for (int i=-n[0]; i < lim1; i++){
        for (int j=-n[1]; j < n[1]+1; j++){
            for (int k=-n[2]; k < n[2]+1; k++){
                dlat[0] = i*reclat[0] + j*reclat[3] +k*reclat[6];
                dlat[1] = i*reclat[1] + j*reclat[4] +k*reclat[7];
                dlat[2] = i*reclat[2] + j*reclat[5] +k*reclat[8];
                if (i != 0 || j != 0 || k!=0){
                    r = dlat[0]*dlat[0] + dlat[1]*dlat[1] + dlat[2]*dlat[2];
                    if (r > cutoff*cutoff){
                        continue;
                    }
                    factor = exp(-eta*eta*r/2)/r;
                    for (int iat=0; iat <nat; iat++){
                        kri =dlat[0]*ats[iat*3] + dlat[1]*ats[iat*3+1] + dlat[2]*ats[iat*3+2];
                        for (int jat=iat;jat<nat;jat++){
                            krj =dlat[0]*ats[jat*3] + dlat[1]*ats[jat*3+1] + dlat[2]*ats[jat*3+2];
                            ptr1[iat+nat*jat] = ptr1[iat+nat*jat] + factor*(cos(kri)*cos(krj)+sin(kri)*sin(krj));
                            }}}}}}
    for (int iat = 0; iat < nat; iat ++){
        for (int jat = 0; jat < nat; jat ++){
            ptr1[jat+nat*iat] = ptr1[iat+nat*jat];}}

    Arecip.resize({nat,nat});
    // Areal.reshape({8,8});
    return Arecip;
}

py::array_t<double> ewaldSelf(
                        int nat,
                        double eta){
    py::array_t<double> Aself = py::array_t<double>(nat*nat);
    py::buffer_info buf1 = Aself.request();
    double *ptr1 = (double *) buf1.ptr;
    // for (int i = 0; i < nat*nat ; i++)
    // {
    //     ptr1[i] = 0.0;
    // }
    // std::fill_
    std::fill_n(ptr1,nat*nat,0.0);
    for (int i = 0; i < nat ; i++){
        ptr1[nat*i+i] = -2/(sqrt(2*3.14159265359)*eta);
    }
    Aself.resize({nat,nat});
    return Aself;
}
PYBIND11_MODULE(ewaldCPP, m) {
    m.def("ewaldReal",&ewaldReal);
    m.def("ewaldRecip",&ewaldRecip);
    m.def("ewaldSelf",&ewaldSelf);
}
