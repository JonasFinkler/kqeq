import numpy as np
from ase.io import *
from ase.data import covalent_radii
from ase.units import Bohr
import random
# from quippy.descriptors import Descriptor
from dscribe.descriptors import SOAP
np.random.seed(10)

class kQEqKernel():
    """
    Parameters
    ----------
    Kernel : string
        Kernel function
    Descriptor : string
        descriptor
    descriptor_dict : dict
        dictionary of descriptor specific hyperparameters
    training_set : list
        List of atoms objects containing training data
    training_system_charges : list
        List of charges of training data 
    validation_set : list 
        (Optional) List of atoms objects containing validation data. Used to precompute matrices to accellerate hyperparameter optimization.
    validation_system_charges : list
        List of charges of validation data
    sparse: bool
        Setting training to use full or sparse training set
    sparse_count: int
        Number of training points used for sparsed training (if CURel is selected, number of sparse points is per element)
    sparse_method: string
        Method used to specify representative set "CUR" or "CURel" (default). If the specific points should be picked, use "prepicked" keyword
    perEl : bool
        Setup if a  kernel matrix should have zero or nonzero entries for comparison between different elements

    """
    def __init__(self,
                 multi_SOAP=False, 
                 descriptor_dict=None, 
                 training_set=None, 
                 training_system_charges = None, 
                 validation_set=None,
                 validation_system_charges = None, 
                 perEl = True, 
                 sparse=False, 
                 sparse_count=None, 
                 sparseSelect = None,
                 sparse_method="CURel"):

        self.sparse=None
        self.descriptor_dict = descriptor_dict
        self.multi_SOAP = multi_SOAP
        self.elements = descriptor_dict["species"]
        self.sparse = sparse
        self.sparse_count = sparse_count
        self.perEl = perEl
        if training_set is not None:
            self.training_set = training_set
            self.training_set_descriptors,self.training_set_elements = self.descriptor_atoms(training_set)
            self.nAt_train = len(self.training_set_descriptors)
            if training_system_charges == None:
                self.training_system_charges = [0 for temp in training_set]   
            else: 
                self.training_system_charges = training_system_charges
        else:
            self.training_set_descriptors = None
            self.training_set = None
            self.training_system_charges = None
            self.training_set_elements = None
        if validation_set is not None:
            self.validation_set_descriptors = self.descriptor_atoms(validation_set)
        else:
            self.validation_set_descriptors = None

        self.validation_set = validation_set
        if validation_system_charges == None and validation_set is not None:
            self.validation_system_charges = [0 for temp in validation_set]
        else: 
            self.validation_system_charges = validation_system_charges
        if self.sparse == True and training_set is not None:
            self.sparse_method = sparse_method
            if sparse_count == None:
                if  self.nAt_train  < 1000:
                    self.sparse_count =  self.nAt_train
                    print("Training set has less than 1000 points, training will be done with the full set")
                else:
                    self.sparse_count = 1000
            else:
                self.sparse_count = int(sparse_count)
#            FPS is not updated, does not work
#            if sparse_method == "FPS":
#                self.sparse_points = self._farthest_point(self.K_train,self.sparse_count,1)
#                Kernel._create_representationFPS(self.sparse_points)
            if sparse_method == "CUR":
                self.sparse_points = self._create_representationCUR(self.sparse_count)
                #self.Kernel._create_representationCUR(self.sparse_count)
            elif sparse_method == "CURel":
                self.sparse_points = self._create_representationCUR_elements(self.sparse_count)
                #self.Kernel._create_representationCUR_elements(self.sparse_count)
            # elif sparse_method == "CURcov":
            #     self.sparse_points = self._create_CUR_covariance(self.sparse_count)
            # elif sparse_method == "CURcovel":
            #     self.sparse_points = self._create_CUR_covariance_elements(self.sparse_count)
            elif sparse_method == "prepicked":
                self.sparse_points = sparseSelect
                self._create_representationPrepicked(sparseSelect)
            elif sparse_method == None:
                pass    
            else:
                print("Specify sparse method!")
        elif self.sparse == False and training_set is not None:
            self.representing_set_descriptors = self.training_set_descriptors
            self.representing_set_elements = self.training_set_elements
        
        #######################################################################
        if self.training_set is not None:
            self.train_descs = {}
            self.repres_descs = {}
            ldesc = len(self.training_set_descriptors[0])
            for el in self.elements:

                self.repres_descs[el] = []
                if self.perEl == True:
                    self.train_descs[el] = []
                    for countTrain in range(len(self.training_set_descriptors)):
                        if el == self.training_set_elements[countTrain]:
                            self.train_descs[el].append(self.training_set_descriptors[countTrain])
                        else:
                            self.train_descs[el].append(np.zeros(ldesc))
                    self.train_descs[el] = np.array(self.train_descs[el])
                else:
                    self.train_descs[el] = self.training_set_descriptors
                for countRepre in range(len(self.representing_set_descriptors)):
                    if el == self.representing_set_elements[countRepre]:
                        self.repres_descs[el].append(self.representing_set_descriptors[countRepre])
                    else:
                        self.repres_descs[el].append(np.zeros(ldesc))
                self.repres_descs[el] = np.array(self.repres_descs[el])
        #######################################################################



    def _create_representationPrepicked(self,picked):
        if self.multi_SOAP == False:
            self.representing_set_descriptors = [self.training_set_descriptors[pos] for pos in picked]
        elif self.multi_SOAP == True:
            self.representing_set_descriptors = []
            for ind in range(len(self.training_set_descriptors)):
                self.representing_set_descriptors.append([self.training_set_descriptors[ind][pos] for pos in picked])
        self.sparse = True

    def CURdecomposition(self,sparse_count,mat = None, k=None):
        """
        Function for selection and creation of representative set based on CUR decomposition 
        
        
        Parameters
        ----------
        sparse_count : 
            number of data points in represetative set
            
        Returns
        -------
        picked : list 
            list of index for representative set 
            
        """
        if k == None:
            k = int(np.linalg.matrix_rank(mat)/2) # not doable for multi soap
        mat = np.array(mat).T
        _,_,V = np.linalg.svd(mat) # not doable for multi soap
        V = V[:k,:]
        vk = V.T
        pis = (1/k)*np.sum(vk**2,axis=1)
        p = [min(1,a*sparse_count) for a in pis]
        picked = np.flip(np.argsort(p))[:sparse_count]
        # self.representing_set_descriptors = [self.training_set_descriptors[pos] for pos in picked]
        # self.sparse = True
        return picked

    def _create_CUR_covariance(self,sparse_count,zeta = 2):
        covariance = np.matmul(self.training_set_descriptors,np.transpose(self.training_set_descriptors))**zeta
        picked = self.CURdecomposition(sparse_count=sparse_count,mat=covariance)
        self.representing_set_descriptors = [self.training_set_descriptors[pos] for pos in picked]
        self.sparse = True
        return picked
    
    def _create_CUR_covariance_elements(self,sparse_count,zeta=2):
        specs = []
        for mol in self.training_set:
            for atom in mol:
                specs.append(atom.symbol)
        soapVecs = {a:[] for a in self.descriptor_dict['species']}
        soapIds = {a:[] for a in self.descriptor_dict['species']}
        for id, el in enumerate(specs):
            soapVecs[el].append(self.training_set_descriptors[id])
            soapIds[el].append(id)
        self.representing_set_descriptors = []
        pickedAll = []
        for element in self.descriptor_dict["species"]:
            covariance = np.matmul(soapVecs[element],np.transpose(soapVecs[element]))**zeta
            picked = self.CURdecomposition(sparse_count=sparse_count,mat=covariance)
            self.representing_set_descriptors.extend([soapVecs[element][pos] for pos in picked]) # this has to be changed for multi SOAP
            pickedAll.extend([soapIds[element][pos] for pos in picked])
        self.sparse = True
        return pickedAll

    
    def _create_representationCUR(self,sparse_count):
        picked = self.CURdecomposition(sparse_count=sparse_count,mat=self.training_set_descriptors)
        self.representing_set_descriptors = [self.training_set_descriptors[pos] for pos in picked]
        self.representing_set_elements = [self.training_set_elements[pos] for pos in picked]
        self.sparse = True
        return picked
    
    def _create_representationCUR_elements(self,sparse_count):
        specs = []
        for mol in self.training_set:
            for atom in mol:
                specs.append(atom.symbol)
        soapVecs = {a:[] for a in self.descriptor_dict['species']}
        soapIds = {a:[] for a in self.descriptor_dict['species']}
        for id, el in enumerate(specs):
            soapVecs[el].append(self.training_set_descriptors[id])
            soapIds[el].append(id)
        self.representing_set_descriptors = []
        self.representing_set_elements = []
        pickedAll = []
        for element in self.descriptor_dict["species"]:
            picked = self.CURdecomposition(sparse_count=sparse_count,mat=soapVecs[element])
            self.representing_set_descriptors.extend([soapVecs[element][pos] for pos in picked]) # this has to be changed for multi SOAP
            self.representing_set_elements.extend([element for pos in picked])
            pickedAll.extend([soapIds[element][pos] for pos in picked])
        self.sparse = True
        return pickedAll
    


class SOAPKernel(kQEqKernel):
    def __init__(self,multi_SOAP=False,descriptor_dict=None,
                training_set=None, training_system_charges = None,
                validation_set=None,validation_system_charges = None,
                perEl = True, sparse=False, sparse_count=None, sparseSelect = None,
                sparse_method="CUR",zeta=2):
        super().__init__(multi_SOAP=multi_SOAP,
                         descriptor_dict=descriptor_dict,
                         training_set=training_set, 
                         training_system_charges = training_system_charges,
                         validation_set=validation_set,
                         validation_system_charges = validation_system_charges,
                         perEl = perEl,
                         sparse = sparse,
                         sparse_count=sparse_count,
                         sparseSelect = sparseSelect,
                         sparse_method=sparse_method)
        self.zeta = zeta

    def descriptor_atoms(self,molecules):
        """
        SOAP encodes regions of atomic geometries by using a local expansion of a gaussian smeared atomic density with orthonormal functions based on spherical harmonics and radial basis functions.
        
        Parameters
        ----------
        molecules :  list
            list of ase atoms objects
        species : list
             The chemical species as a list of atomic numbers or as a list of chemical symbols.
        rcut : float
            A cutoff for local region in angstroms. Should be bigger than 1 angstrom.
        nmax : int  
            The number of radial basis functions.
        lmax : int 
            The maximum degree of spherical harmonics.
        periodic : bool 
            Determines whether the system is considered to be periodic
        sigma : float
            The standard deviation of the gaussians used to expand the atomic density.
            
        Returns
        -------
        soap_atoms : list
            list of normalized soap vectors.
        """
        element_list = []
        soap = SOAP(
            species=self.descriptor_dict['species'],
            periodic=self.descriptor_dict['periodic'],
            rcut=self.descriptor_dict['rcut'],  
            nmax=self.descriptor_dict['nmax'],
            sigma=self.descriptor_dict['sigma'],
            lmax=self.descriptor_dict['lmax'])
        soap_atoms=[]
        for mol in molecules:
            element_list.extend([a.symbol for a in mol])
            soap_vec = soap.create(mol)
            normal_soap_vector = []
            for sv in soap_vec:
                
                norm = np.linalg.norm(sv)
                normal_soap_vector.append(sv/norm)
            soap_atoms.extend(normal_soap_vector) 
        return soap_atoms, element_list

    def kernel_matrix(self,mol_set1=None,mol_set2=None,kerneltype='general'):
        """
        Evaluation of a kernel matrix

        Parameters
        ----------
        mol_set1 :  list
            list of ase atoms objects
        mol_set2 : list
            list of ase atoms objects
        kerneltype : string
            Set up of the type of kernel ("training" or "prediction")
            
        Returns
        -------
        K : numpy 2D array
            Kernel matrix between the representative set and points for the prediction
        K_nm : numpy 2D array
            Kernel matrix between the representative set and the trainig set
        K_mm : numpy 2D array
            Kernel matrix between the representative set points
        """
        zeta = self.zeta
        if kerneltype=='general':
            if mol_set2 == None:
                desc1 = self.descriptor_atoms(mol_set1)
                dim1  = len(desc1)
                K = np.matmul(desc1,np.transpose(desc1))**zeta
            else:
                desc1 = self.descriptor_atoms(mol_set1)
                desc2 = self.descriptor_atoms(mol_set2)
                dim1  = len(desc1)
                dim2  = len(desc2)
                K = np.matmul(desc1,np.transpose(desc2))**zeta
            return K
        elif kerneltype=='predicting':
            desc1,elems1 = self.descriptor_atoms(mol_set1)
            ldesc = len(desc1[0])
            el_descs = {}
            
            for el in self.elements:
                el_descs[el] = []
                if self.perEl == True:
                    for countRepre in range(len(desc1)):
                        if el == elems1[countRepre]:
                            el_descs[el].append(desc1[countRepre])
                        else:
                            el_descs[el].append(np.zeros(ldesc))
                    el_descs[el] = np.array(el_descs[el])
                elif self.perEl == False:
                    el_descs[el] = desc1
            K = np.zeros((len(desc1),len(self.representing_set_descriptors)))
            for el in self.elements:
                K_temp = np.matmul(np.array(el_descs[el]),np.transpose(np.array(self.repres_descs[el])))**zeta
                K += K_temp
            return K
        elif kerneltype=='training':
            K_nm = np.zeros((len(self.training_set_descriptors),len(self.representing_set_descriptors)))
            K_mm = np.zeros((len(self.representing_set_descriptors),len(self.representing_set_descriptors)))
            for el in self.elements:
                K_nm += np.matmul(self.train_descs[el],np.transpose(self.repres_descs[el]))**zeta
                K_mm += np.matmul(self.repres_descs[el],np.transpose(self.repres_descs[el]))**zeta   
            return K_nm, K_mm
            # print(train_descs)

    def _calculate_function(self,mol_set1,zeta=2):
        desc1,der1 = self._descr_deriv_atoms([mol_set1],deriv="numerical")
        desc_train = self.representing_set_descriptors
        normal_soap_vector = []
        norms = []
        for sv in desc1:
            norm = np.linalg.norm(sv)
            normal_soap_vector.append(sv/norm)
            norms.append(norm)
        norms = np.array(norms)
        norms2 = norms**2
        K_final = np.matmul(normal_soap_vector,np.transpose(desc_train))**zeta     
        deriv_final = np.moveaxis(der1,[0,1],[2,0])

        normal_soap_vector = np.array(normal_soap_vector)
        hold = (np.einsum('ij,arkj->arik',desc1,deriv_final,optimize="greedy"))
        vd = np.einsum('akii->aki',hold,optimize="greedy")
        r1 = np.einsum('akl,lj,l->aklj',vd,desc1,1/norms,optimize="greedy")
        f1 = np.einsum('aklj,l->aklj',deriv_final,norms,optimize="greedy")
        normDer = np.einsum('aklj,l->aklj',f1-r1,1/norms2,optimize="greedy")
        #t1 = np.einsum('ij,lj->il',normal_soap_vector,desc_train,optimize="greedy")**(zeta-1)
        #t2 = np.einsum('akij,lj->akil',normDer,desc_train,optimize="greedy")
        #dKdr = zeta*np.einsum('akij,ij->akij',t2,t1,optimize="greedy")
#        t1 = np.matmul(normal_soap_vector,np.transpose(desc_train))**(zeta-1)
        t1 = np.einsum('ij,lj->il',normal_soap_vector,desc_train,optimize="greedy")**(zeta-1)
        dKdr = zeta*np.einsum('akij,lj,il->akil',normDer,desc_train,t1,optimize=['einsum_path', (0, 1), (0, 1)])        
        return K_final,dKdr

    def _descr_deriv_atoms(self,molecule, deriv = "numerical"):
        soap = SOAP(
            species=self.descriptor_dict['species'],
            periodic=self.descriptor_dict['periodic'],
            rcut=self.descriptor_dict['rcut'],
            nmax=self.descriptor_dict['nmax'],
            sigma=self.descriptor_dict['sigma'],
            lmax=self.descriptor_dict['lmax']) 
        soap_d, soap_vec = soap.derivatives(molecule,method=deriv,attach=True) #analytical derivatives in SOAPs are not usable with 1.2.0 version
        return soap_vec, soap_d   
