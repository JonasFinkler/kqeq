from kqeq.ewaldCPP import ewaldReal, ewaldRecip, ewaldSelf
# import kqeq.ewaldCPP
import numpy as np

ewaldSummationPrecision = 1E-8

def eemMatrixEwald(nAt,xyz,lattice,atsize,hard):
    eta = getOptimalEtaMatrix(lattice)
    eta = np.max([eta,np.max(atsize)])
    cutoff = getOptimalCutoffReal(eta,ewaldSummationPrecision)
    n = getNcells(lattice,cutoff)
    reclat = recLattice(lattice)
    V = np.linalg.det(lattice)
    Real = ewaldReal(nAt,xyz,lattice,atsize,hard,n,eta,cutoff)
    # print("done")
    Recip = ewaldRecip(nAt,xyz,reclat,n,eta,cutoff,V)
    # print("done2")
    Recip = Recip/V *4*np.pi
    Aself = ewaldSelf(nAt,eta)
    # print("done32")
    res =  Recip + Real + Aself
    return res



def getOptimalEtaMatrix(lat):
    return 1/np.sqrt(2*np.pi)*np.linalg.det(lat)**(1/3)

def getOptimalCutoffReal(eta,A):
    r = np.sqrt(2)*eta*np.sqrt(-np.log(A))
    return r

def getOptimalCutoffRecip(eta,A):
    r = np.sqrt(2)/eta*np.sqrt(-np.log(A))
    return r

def recLattice(At):
    # At = At.T
    B = (np.linalg.inv(At.T)*(2*np.pi))
    return B

def getNcells(lat,cutoff):
    proj = np.zeros(3)
    # axb = np.cross(lat[:,0],lat[:,1])
    axb = np.cross(lat[0],lat[1])
    axb = axb / np.sqrt(np.sum(axb**2))
    # axc = np.cross(lat[:,0], lat[:,2])
    axc = np.cross(lat[0], lat[2])
    axc = axc / np.sqrt(np.sum(axc**2))
    # bxc = np.cross(lat[:,1], lat[:,2])
    bxc = np.cross(lat[1], lat[2])
    bxc = bxc / np.sqrt(np.sum(bxc**2))
    proj[0] = np.dot(lat[0], bxc)
    proj[1] = np.dot(lat[1], axc)
    proj[2] = np.dot(lat[2], axb)
    n = np.ceil(cutoff/abs(proj))
    n = [int(a) for a in n]
    return n 
