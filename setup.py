import sys
from distutils.ccompiler import new_compiler
from distutils.sysconfig import customize_compiler
from setuptools import setup, find_packages, Extension
import subprocess


class get_pybind_include(object):
    """Helper class to determine the pybind11 include path
    The purpose of this class is to postpone importing pybind11
    until it is actually installed, so that the ``get_include()``
    method can be invoked. """

    def __init__(self, user=False):
        try:
            import pybind11
        except ImportError:
            if subprocess.call([sys.executable, '-m', 'pip', 'install', 'pybind11']):
                raise RuntimeError('pybind11 install failed.')

        self.user = user

    def __str__(self):
        import pybind11
        return pybind11.get_include(self.user)
    


cpp_extra_link_args = []
cpp_extra_compile_args = [
    "-std=c++11",                   # C++11
    "-O3",                          # O3 optimizations
    "-I dependencies/eigen/Eigen/"  # Eigen dependency
]

extensions = [
    # The SOAP, MBTR, ACSF and utils C++ extensions, wrapped with pybind11
    Extension(
        'kqeq/ewaldCPP',
        ["kqeq/ewald.cpp"],
        include_dirs=[
            get_pybind_include(),
            get_pybind_include(user=True),
        ],
        language='c++',
        extra_compile_args=cpp_extra_compile_args + ["-fvisibility=hidden"],  # the -fvisibility flag is needed by pybind11
        extra_link_args=cpp_extra_link_args,
    )
]


setup(
    name='kqeq',
    version='2.0',
    packages=['kqeq'],
    url='https://jmargraf.gitlab.io/kqeq/',
    license='The software is licensed under the MIT License',
    author='Johannes Margraf, Martin Vondrak',
    author_email='margraf@fhi.mpg.de',
    description='A Python implementation of the kernel Charge Equilibration method, allows training and using physics-based machine-learning models for predicting charge distributions in molecules and materials.',
    install_requires=["pybind11>=2.2",'numpy','scipy','ase','dscribe'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",],
    ext_modules = extensions,
)
